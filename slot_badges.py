from __future__ import division
import json
import math
import requests
from anybadge import Badge
from itertools import chain


SLOT_BADGE_PLATFORMS = ["x86_64-centos7-gcc8-opt"]
COLORS = {
    "pass": ("#476c47", "#398439"),
    "warn": ("#a28152", "#f0ad4e"),
    "fail": ("#803f3d", "#ac2925"),
}


def progress(x):
    x = list(map(bool, x))
    fraction = sum(x) / len(x)
    if fraction > 0 and fraction < 0.01:
        # if we just started, return a non-zero value
        return 0.01
    return int(math.floor(fraction * 100))


def badge(prefix, status, label, color):
    p = progress(status)
    c = COLORS[color][p == 100]
    label = label + ("  " if len(label) <= 6 else " " if len(label) <= 12 else "")
    return Badge(prefix + " {}%".format(p), label, default_color=c)


def extract_status(data):
    projects = data["config"]["projects"]
    platforms = data["config"]["platforms"]
    status = {"build": {}, "tests": {}}
    problems = {"retcode": {}, "errors": {}, "warnings": {}, "tests": {}}
    for platform in platforms:
        for project in projects:
            if project["disabled"]:
                continue
            key = (platform, project["name"])
            build = data["builds"].get(platform, {}).get(project["name"], {})
            status["build"][key] = bool(build.get("completed"))
            if build.get("completed"):
                problems["warnings"][key] = build["warnings"]
                problems["errors"][key] = build["errors"]
                problems["retcode"][key] = int(build["retcode"] != 0)
            if not project.get("no_test", False):
                tests = data["tests"].get(platform, {}).get(project["name"], {})
                status["tests"][key] = bool(tests.get("completed"))
                if tests.get("completed"):
                    for result, test_names in tests["results"].items():
                        for test_name in test_names:
                            problems["tests"][key + (test_name,)] = int(
                                result != "PASS"
                            )

    return status, problems


def filter_platforms(problems, platforms):
    return {
        typ: {key: n for key, n in probs.items() if key[0] in platforms}
        for typ, probs in problems.items()
    }


def build_slot_badge(status, problems):
    retcodes = sum(problems["retcode"].values())
    errors = sum(problems["errors"].values())
    warnings = sum(problems["warnings"].values())
    if errors or retcodes:
        label, color = "failing: {} errors".format(errors), "fail"
    elif warnings:
        label, color = "passing: {} warnings".format(warnings), "warn"
    else:
        label, color = "passing", "pass"
    return badge("build", status["build"].values(), label, color)


def tests_slot_badge(status, problems):
    failing = sum(problems["tests"].values())
    passing = len(problems["tests"]) - failing
    label = "✔ {} | ✘ {}".format(passing, failing)  # ✔ 20 | ✘ 1 | ➟ 1
    color = "fail" if failing else "pass"
    return badge("tests", status["tests"].values(), label, color)


def diff_status(status1, status2):
    return {
        job: {
            k: status1[job].get(k, True) and status2[job].get(k, True)
            for k in chain(status1[job].keys(), status2[job].keys())
        }
        for job in ["build", "tests"]
    }


def diff_problems(old, new, status_diff):
    diff = {}
    for typ in new:
        status = status_diff["tests"] if typ == "tests" else status_diff["build"]
        diff[typ] = (
            # new problems
            sum(
                n > old[typ].get(key, 0)
                for key, n in new[typ].items()
                if status[key[:2]]
            ),
            # fixed problems
            sum(
                n < old[typ].get(key, 0)
                for key, n in new[typ].items()
                if status[key[:2]]
            ),
            # removed
            sum(key not in new[typ] for key in old[typ] if status[key[:2]]),
        )
    return diff


def format_diff(triplet, symbols=["+", "−", "⨯"]):
    return " ".join(s + str(n) for s, n in zip(symbols, triplet) if n)


def build_diff_badge(status, diff):
    parts = []
    if format_diff(diff["errors"]):
        parts.append(format_diff(diff["errors"]))
    if format_diff(diff["warnings"]):
        parts.append(format_diff(diff["warnings"]))
    label = " | ".join(parts) if parts else "OK"
    if diff["errors"][0] or diff["retcode"][0]:
        color = "fail"
    elif diff["warnings"][0]:
        color = "warn"
    else:
        color = "pass"
    return badge("𝚫 build", status["build"].values(), label, color)


def tests_diff_badge(status, diff):
    if any(diff["tests"]):
        label = format_diff(diff["tests"], ["−", "+", "⨯"])
        color = "fail" if diff["tests"][0] else "warn"
    else:
        label, color = "OK", "pass"
    return badge("𝚫 tests", status["tests"].values(), label, color)


if __name__ == "__main__":
    # no diffs
    slot_ref, slot = "lhcb-master-ref.25", "lhcb-master-mr.21"
    # new failing tests
    slot_ref, slot = "lhcb-master-ref.24", "lhcb-master-mr.20"
    # in progress
    slot_ref, slot = "lhcb-head.2408", "lhcb-head.2409"

    print(
        "https://lhcb-nightlies.cern.ch/nightly/compare/{}/{}".format(
            slot.replace(".", "/"), slot_ref.replace(".", "/")
        )
    )

    def get_slot(flavour, slot):
        fn = "testdata/{}-{}.json".format(flavour, slot)
        try:
            with open(fn) as f:
                return json.load(f)
        except OSError:
            pass
        r = requests.get(
            "https://lhcb-couchdb.cern.ch/nightlies-{}/{}".format(flavour, slot),
            verify=False,
        )
        r.raise_for_status()
        with open("testdata/{}-{}.json".format(flavour, slot), "w") as f:
            f.write(r.text)
        return json.loads(r.text)

    status, problems = extract_status(get_slot("nightly", slot))
    status_refs, problems_ref = extract_status(get_slot("nightly", slot_ref))

    problems_subset = filter_platforms(problems, SLOT_BADGE_PLATFORMS)
    build_slot_badge(status, problems_subset).write_badge(
        "ex/build-slot-badge-{}.svg".format(slot), True
    )
    tests_slot_badge(status, problems_subset).write_badge(
        "ex/test-slot-badge-{}.svg".format(slot), True
    )

    diff_s = diff_status(status_refs, status)
    diff = diff_problems(problems_ref, problems, diff_s)
    build_diff_badge(diff_s, diff).write_badge(
        "ex/build-diff-badge-{}.svg".format(slot), True
    )
    tests_diff_badge(diff_s, diff).write_badge(
        "ex/tests-diff-badge-{}.svg".format(slot), True
    )

